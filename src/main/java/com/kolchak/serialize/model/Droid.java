package com.kolchak.serialize.model;

import java.io.Serializable;

public class Droid implements Serializable {
    private String model;
    private int version;
    private transient int capacity;
    private static double weight = 123.45;

    public Droid(String model, int version, int capacity) {
        this.model = model;
        this.version = version;
        this.capacity = capacity;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public static double getWeight() {
        return weight;
    }

    public static void setWeight(double weight) {
        Droid.weight = weight;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "model='" + model + '\'' +
                ", version=" + version +
                ", capacity=" + capacity +
                '}';
    }
}
