package com.kolchak.serialize.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ship implements Serializable {
    private String shipName = "StarTrack";
    private List<Droid> droidList;

    public Ship() {
        droidList = new ArrayList<>();
        droidList.add(new Droid("Qwe", 23,100));
        droidList.add(new Droid("Asd", 37,1231));
        droidList.add(new Droid("Zxc", 88,345));
        droidList.add(new Droid("Poi", 123,23));
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public List<Droid> getDroidList() {
        return droidList;
    }

    public void setDroidList(List<Droid> droidList) {
        this.droidList = droidList;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "shipName='" + shipName + '\'' +
                ", droidList=" + droidList +
                '}';
    }
}
