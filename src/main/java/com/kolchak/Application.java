package com.kolchak;

import com.kolchak.view.MyView;
import org.apache.logging.log4j.*;

import java.io.IOException;

public class Application {
    public static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) throws IOException, InterruptedException {

        new MyView().show();

    }
}
