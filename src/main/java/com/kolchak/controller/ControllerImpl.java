package com.kolchak.controller;

import com.kolchak.Application;
import com.kolchak.compareReading.FileToCompare;
import com.kolchak.serialize.model.Ship;
import org.apache.logging.log4j.*;

import java.io.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ControllerImpl implements Controller {
    public static Logger logger = LogManager.getLogger(Application.class);
    private Ship ship = new Ship();
    private FileToCompare fileToCompare;
    private Scanner scanner;

    public void writeToFile() {
        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("ShipAndDroids.txt"));
            outputStream.writeObject(ship);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readFromFile() {
        System.out.println(ship);
        try {
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("ShipAndDroids.txt"));
            Ship readShip = (Ship) inputStream.readObject();
            System.out.println(readShip);
            inputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void compareReadingSlow() throws IOException {
        fileToCompare = new FileToCompare();
        int count = 0;
        InputStream inputStream = new FileInputStream(fileToCompare.getFile());
        int read = inputStream.read();
        while (read != -1) {
            read = inputStream.read();
            count++;
        }
        logger.info("Slow - byte by byte.");
        logger.info(count);
        inputStream.close();
    }

    public void compareReadingLimitedSpeed() throws IOException {
        fileToCompare = new FileToCompare();
        int count = 0;
        int bufferSize = 1 * 1024 * 1024;
        DataInputStream dataInputStream = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(fileToCompare.getFile()), bufferSize));
        while (dataInputStream.readByte() != -1) {
            count++;
        }
        logger.info("Limited speed - 1  MB.");
        logger.info(count);
        dataInputStream.close();
    }

    public void compareReadingHighSpeed() throws IOException {
        fileToCompare = new FileToCompare();
        int count = 0;
        DataInputStream dataInputStream = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(fileToCompare.getFile())));
        while (dataInputStream.readByte() != -1) {
            count++;
        }
        logger.info("High speed, no limits.");
        logger.info(count);
        dataInputStream.close();
    }

    public void readCommentsFromJavaFile() throws IOException {
        System.out.println("Input file name: ");
        scanner = new Scanner(System.in);
        String nextLine = scanner.nextLine();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(nextLine));
        String text = null;
        String st;
        while ((st = bufferedReader.readLine()) != null) {
            text += st /*+"\n"*/;
        }
        Pattern pattern = Pattern.compile("^(\\/\\*\\*)([\\s\\S]*)(\\*\\/)$");
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            logger.info(matcher.group());
        }
    }
}


