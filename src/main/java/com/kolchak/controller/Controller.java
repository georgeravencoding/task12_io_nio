package com.kolchak.controller;

import java.io.IOException;

public interface Controller {
    void writeToFile();

    void readFromFile();

    void compareReadingSlow() throws IOException;

    void compareReadingLimitedSpeed() throws IOException;

    void compareReadingHighSpeed() throws IOException;

    void readCommentsFromJavaFile() throws IOException;
}
