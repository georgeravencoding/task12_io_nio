package com.kolchak.compareReading;

import java.io.File;

public class FileToCompare {
    private File file = new File("OSSTMM.3.pdf");

    public FileToCompare() {
    }

    public File getFile() {
        return file;
    }

    @Override
    public String toString() {
        return "FileToCompare{" +
                "file=" + file +
                '}';
    }
}
